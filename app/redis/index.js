const createClient = require('redis').createClient;
const logger=require('../logger');
let client = null;

(async () => {
    client = createClient({
        url: `redis://${global.config_data.global.redis_url}:${global.config_data.global.redis_port}`
    });
  
    client.on(
        'error', (err) => {
            logger.error(`redis: error connecting to redis://${global.config_data.global.redis_url}:${global.config_data.global.redis_port}`, err);
            process.exit(1);
        }
        
    );
  
    await client.connect();
    logger.info ( `redis: service connected to redis://${global.config_data.global.redis_url}:${global.config_data.global.redis_port}`)

})();


// var client = createClient({
//     url: `redis://${global.config_data.global.redis_url}:${global.config_data.global.redis_port}`
// });

// client.on(
//     'error', (err) => {
//         logger.error(`redis: error connecting to redis://${global.config_data.global.redis_url}:${global.config_data.global.redis_port}`, err);
//         process.exit(1);
//     }
    
// );
// client.connect();
// logger.info ( `redis: service connected to redis://${global.config_data.global.redis_url}:${global.config_data.global.redis_port}`)

module.exports.setkey = async function(key, value, ttl){
    await client.set(key, value, {
        EX: ttl,
        NX: true
    });
}

module.exports.getkey = async function(key){
    let r = await client.get(key);
    return r;
}

module.exports.removekey = async function(key){
    return await client.DEL(key);
}

module.exports.getttl = async function(key){
    return Math.round((await client.PTTL(key))/1000);
}