const { stream } = require('../logger');
const logger=require('../logger'); 
const msgServerError = 'Server error';
const k8s = require('@kubernetes/client-node');
const redisclient=require('../redis');

async function patchHPAminReplicas(hpaname, namespace, minReplicas, api) {
	const patch = [
		{
			"op": "replace",
			"path":"/spec/minReplicas",
			"value": minReplicas
		}
	];
	const options = { "headers": { "Content-type": k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH}};
	await api.patchNamespacedHorizontalPodAutoscaler(hpaname, namespace, patch, undefined, undefined, undefined, undefined, options);
}

async function getCurrentHPAminReplicas(hpaname, namespace,  api){
	const hpa = await api.readNamespacedHorizontalPodAutoscaler(hpaname, namespace);
	return hpa.body.spec.minReplicas;
}

async function getCurrentHPAmaxReplicas(hpaname, namespace,  api){
	const hpa = await api.readNamespacedHorizontalPodAutoscaler(hpaname, namespace);
	return hpa.body.spec.maxReplicas;
}


async function getCurrentDeploymentReplicas(deploymentname, namespace,  api){
	const deployment = await api.readNamespacedDeployment(deploymentname, namespace); // find the deployment and get current deployment replicas
	return deployment.body.spec.replicas; 
}

async function patchDeployment(deploymentname, namespace, minReplicas, api) {
	const patch = [
		{
			"op": "replace",
			"path":"/spec/replicas",
			"value": minReplicas
		}
	];
	const options = { "headers": { "Content-type": k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH}};
	await api.patchNamespacedDeploymentScale(deploymentname, namespace, patch, undefined, undefined, undefined, undefined, options);
}

async function waitHPA(hpaname, namespace, maxwaitsecs,  api){
	let waitstep = 1000; //1000 = 1 sec
	let start = Number(Date.now());
	let end = start + maxwaitsecs * 1000;
	while (Number(Date.now()) < end) {
		try{
			let hpa = await api.readNamespacedHorizontalPodAutoscaler(hpaname, namespace);
			if (hpa.body.status.currentMetrics[0].external != null){
				logger.debug(`waitHPA: metric=${(hpa.body.status.currentMetrics[0]).external.current.value} after ${Math.floor(((Number(Date.now())) - start)/1000)} secs`);
				if ((hpa.body.status.currentMetrics[0]).external.current.value == 1) break;
			}else{
				logger.warn(`waitHPA: metric=unknown after ${Math.floor(((Number(Date.now())) - start)/1000)} secs`);
			}
			await sleep(waitstep);
		}
		catch(err){
				logger.error(`waitHPA: cannot get status of ${hpaname} after ${Math.floor(((Number(Date.now())) - start)/1000)} secs. ${err}`);
				await sleep(waitstep);
		}
	}
}

exports.boosteron = async (req, res) => { 
	const targetNamespaceName = global.config_customer.namespace;
	const targetDeploymentName = global.config_customer.deploy_name;
	const numberOfExtraReplicas = global.config_customer.replicas;
	const targetHPAName = global.config_customer.hpa_name;
	const targetHPANamespaceName = global.config_customer.namespace;
	const redis_keyname = global.config_data.global.redis_key;
	const redis_ttl_secs = global.config_customer.duration;
	const customer = req.body.customer;
	let booster_status = 0;

	await redisclient.getkey(`${redis_keyname}:${customer}`)
	.then((response) => {
		logger.debug(`boosteron: Redis booster_status=${response}`);
		booster_status = response;
	})
	.catch((err) => {
		logger.error(`boosteron: received invalid response from Redis or Redis not ready at all while reading key ${redis_keyname}:${customer} : ${err}`);
		return res.status(503).send(`boosteron: received invalid response from Redis or Redis not ready at all while reading key ${redis_keyname}:${customer}`);
	})

	if (booster_status == 1){
		logger.warn(`boosteron: booster for customer ${customer} is still running. TTL of booster metric is ${await redisclient.getttl(`${redis_keyname}:${customer}`)} seconds`)
		return res.status(304).send();
	}

	logger.debug(`boosteron: started with deployment=${targetDeploymentName}, deploymentnamespace=${targetNamespaceName}, extrareplicas=${numberOfExtraReplicas}, redis_ttl_secs=${redis_ttl_secs} hpa=${targetHPAName}, hpanamespace=${targetHPANamespaceName}`);

	
	//create k8s client
	const kc = new k8s.KubeConfig();
	if (process.env.NODE_ENV == 'development'){
		kc.loadFromDefault();
	}else{
		kc.loadFromCluster();
	}

	k8sApi = kc.makeApiClient(k8s.AppsV1Api);
	const k8sAutoscalingAPI = kc.makeApiClient(k8s.AutoscalingV2beta2Api);
	//set new metric for HPA
	await redisclient.setkey(`${redis_keyname}:${customer}`, 1, redis_ttl_secs);
	logger.debug(`boosteron: booster metric set to 1 with a TTL of ${redis_ttl_secs} seconds`);
	logger.debug("boosteron: sending http response 200")
	res.status(200).send();  //immediatly send the response to prevent blocking the client

	try {
		//Get current deployment replicas
		let originalDeploymentMinReplicas = await getCurrentDeploymentReplicas (targetDeploymentName, targetNamespaceName, k8sApi)
		let newDeploymentReplicas = originalDeploymentMinReplicas + numberOfExtraReplicas;
		//Get HPA maxReplicas
		let HPAmaxReplicas = await getCurrentHPAmaxReplicas(targetHPAName, targetHPANamespaceName, k8sAutoscalingAPI)
		//Check if originalDeploymentMinReplicas + numberofExtraReplicas is lower or equal to HPAmaxReplicas
		if (newDeploymentReplicas > HPAmaxReplicas){
			logger.warn(`boosteon: HPA max replicas is ${HPAmaxReplicas} and it's lower than booster's requested replicas that is ${newDeploymentReplicas}. Setting booster's replicas to ${HPAmaxReplicas}`);
			newDeploymentReplicas = HPAmaxReplicas;
		}
		logger.debug(`boosteron: newDeploymentReplicas=${newDeploymentReplicas}`);
		//get current HPA minReplicas
		let originalHPAMinReplicas = await getCurrentHPAminReplicas (targetHPAName, targetHPANamespaceName, k8sAutoscalingAPI)
		logger.debug(`boosteron: originalHPAMinReplicas=${originalHPAMinReplicas}`);
		//patch HPA with temporary minReplicas
		//corner case: HPA maxReplicas is lower than desired HPA minReplicas. Watchout when configuring HPA and booster values => HPAminReplicas + boosterValue <= HPAmaxReplicas 
		await patchHPAminReplicas(targetHPAName, targetHPANamespaceName, newDeploymentReplicas, k8sAutoscalingAPI);
		logger.debug(`boosteron: patchHPAminReplicas completed. HPA minReplicas set to ${newDeploymentReplicas}`);
		//Scale deployment
		await patchDeployment(targetDeploymentName, targetNamespaceName, newDeploymentReplicas, k8sApi);
		logger.debug(`boosteron: patchDeployment completed. Deployment replicas set to ${newDeploymentReplicas}`);
		//wait for HPA to aquire metric value
		await waitHPA(targetHPAName, targetHPANamespaceName, 20, k8sAutoscalingAPI);
		logger.debug(`boosteron: waitHPA completed`);
		//patch HPA min replicas with original value
		await patchHPAminReplicas(targetHPAName, targetHPANamespaceName, originalHPAMinReplicas, k8sAutoscalingAPI);
		logger.debug(`boosteron: patchHPAminReplicas completed. HPA minReplicas reset to ${originalHPAMinReplicas}`);
		logger.srvconsoledir(req,start=0);
		//return res.status(200).send(); //response should be sent here but is sent in advance to prevent blocking the client sending the request
	}
	catch(err){
		logger.srvconsoledir(req,start=0,`err=${err}, statuscode=${err.statusCode}, mess=${err.response.statusMessage}` )
		res.status(500).send({ message: msgServerError})	
	}
	// scaledeployment(targetNamespaceName, targetDeploymentName, numberOfExtraReplicas, kc)
	// .then(() => {
	// 	logger.debug(`Dispatcher: ${targetDeploymentName} in namespace: ${targetNamespaceName} is scaled to ${numberOfExtraReplicas} and metric set to 1`);
	// 	logger.srvconsoledir(req,start=0);
	// 	return res.status(200).send();
	// })
	// .catch((err) => {
	// 	logger.srvconsoledir(req,start=0,err);
	// 	res.status(500).send({ message: msgServerError})	
	// })
}

function sleep(ms) {
   return new Promise((resolve) => {
      setTimeout(resolve, ms);
   });
}

exports.boosteroff = async (req, res) => {
	const targetNamespaceName = 'dispatcher';
	const targetDeploymentName = 'dispatcher-api-deploy';
	const redis_keyname = global.config_data.global.redis_key;
	const customer = req.body.customer;

	logger.debug(`boosteroff: started with deployment=${targetDeploymentName}, deploymentnamespace=${targetNamespaceName}`);
	
	//create k8s client
	const kc = new k8s.KubeConfig();
	kc.loadFromDefault();
	const k8sApi = kc.makeApiClient(k8s.AppsV1Api);

	await redisclient.removekey(`${redis_keyname}:${customer}`)
	.then((redisreply) => {
		logger.debug(`boosteroff: Redis removed ${redisreply} keys succesfully`);
	})
	.catch((err) => {
		logger.error(`boosteroff: received invalid response from Redis or Redis not ready at all while removing key ${redis_keyname}:${customer} : ${err}`);
	})

	try {
		await patchDeployment(targetDeploymentName, targetNamespaceName, 1, k8sApi);
		logger.debug(`boosteroff: patchDeployment completed`);
	}
	catch(err){
		logger.srvconsoledir(req,start=0,err);
		res.status(500).send(msgServerError)
	}
	logger.srvconsoledir(req,start=0);
	return res.status(200).send();
}
