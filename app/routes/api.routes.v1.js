module.exports = myapp => {
  const routerapp =      require("express").Router();
  const routermetrics = require("express").Router();
  const loadconfig = require('../boost.config/').loadconfig;
  const apiexternal = require("../logic/api.logic.booster");
  const apihealth = require("../logic/health");

  routerapp.use(loadconfig);

  routerapp.post("/booster",      apiexternal.boosteron );
  routerapp.post("/boosteroff",      apiexternal.boosteroff );
  myapp.use('/api/v1', routerapp);

  routermetrics.get("/health", apihealth.health);
  myapp.use('/', routermetrics);
};