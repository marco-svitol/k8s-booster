const http = require('http');

module.exports = function(app, next) {
  return http.createServer(app)
  .listen(process.env.K8SBOOSTER_SERVERPORT, () => {
    next();
  });
}
